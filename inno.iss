[Setup]
AppId={{23C510D9-5C41-4AE4-8C06-1170491FD78E}
AppName=OSWSDT
AppVersion=1
AppPublisher=Bixilon
AppPublisherURL=https://gitlab.bixilon.de/oswsdt/client/
AppSupportURL=https://gitlab.bixilon.de/oswsdt/client/
AppUpdatesURL=https://gitlab.bixilon.de/oswsdt/client/
DefaultDirName={pf}\OSWSDT
DisableDirPage=yes
DefaultGroupName=OSWSDT
DisableProgramGroupPage=yes
OutputBaseFilename=setup
Compression=lzma/ultra64
SolidCompression=yes
MinVersion=0,6.0
InternalCompressLevel=ultra64
CompressionThreads=2

[Files]
Source: "..\python-3.8.2-amd64.exe"; DestDir: "{tmp}"; DestName: "python-3.8.2-amd64.exe"
Source: ".\client.py"; DestDir: "{app}\client"
Source: ".\classes\Application.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\BackgroundStuff.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\Command.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\Config.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\Log.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\PackageUtil.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\Protocol.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\Setup.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\SocketStuff.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\classes\Tools.py"; DestDir: "{app}\client\classes"; Flags: ignoreversion
Source: ".\NSSM\nssm.exe"; DestDir: "{app}"; DestName: "nssm.exe"
Source: ".\config\config.yml"; DestDir: "{app}\config"; DestName: "config.yml"; Permissions: system-full
Source: ".\config\server.crt.pem"; DestDir: "{app}\config"; Permissions: system-full

[Run]
Filename: "{tmp}\python-3.8.2-amd64.exe"; Parameters: "/quiet InstallAllUsers=1 PrependPath=1"; Description: "Installs Python"
Filename: "C:\Program Files\Python38\python.exe"; Parameters: "-m pip install --upgrade pip"; WorkingDir: "C:\Program Files\Python38"; Flags: runhidden
Filename: "C:\Program Files\Python38\python.exe"; Parameters: "-m pip install PyYAML pyOpenSSL winapps pywin32 wmi"; WorkingDir: "C:\Program Files\Python38"; Flags: runhidden
Filename: "{app}\nssm.exe"; Parameters: "install OSWSDT ""C:\Program Files\Python38\python.exe"" client.py"
Filename: "{app}\nssm.exe"; Parameters: "set OSWSDT AppDirectory ""{app}"""
Filename: "net.exe"; Parameters: "start OSWSDT"; Flags: runhidden
;Filename: "del"; Parameters: "{app}\python-3.8.2-amd64.exe"; Flags: postinstall shellexec; Description: "Delete Python installer"

[Dirs]
Name: "{app}\client"; Attribs: system
Name: "{app}\client\classes"
Name: "{app}\config"

[UninstallRun]
Filename: "{app}\nssm.exe"; Parameters: "remove oswsdt confirm"
