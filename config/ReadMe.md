## config

This is the client config folder. It should be protected (so that normal users do not have any access to it). 

In this folder is the config.yml file. In it are some settings like server address, port,...


There are also 3 certificate files. 1 of them is the certificate, the other one the key. The 3rd is the server certificate.
The client certificate (client.crt.pem) must also be on the server.