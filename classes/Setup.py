import os
import shutil

from OpenSSL import crypto

import client
from classes import Log
from classes import Tools

certs_copied = False


def createCerts():
    # create a key pair
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)

    # create a self-signed cert
    cert = crypto.X509()
    cert.get_subject().C = "DE"
    cert.get_subject().ST = "Munich"
    cert.get_subject().L = "Munich"
    cert.get_subject().O = "OSWSDT Client"
    cert.get_subject().OU = "OSWSDT Client"
    cert.get_subject().CN = "127.0.0.1"
    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, 'sha1')

    with open(client.CONFIG_FOLDER + "client.crt.pem", "wb") as f:
        f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
        f.close()
    with open(client.CONFIG_FOLDER + "client.key.pem", "wb") as f:
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k))
        f.close()
    Log.info("Client certificate generated. Please put it onto the server!")
    global certs_copied
    certs_copied = True


def setup():
    files = {"config.yml", "server.crt.pem", "client.crt.pem", "client.key.pem"}
    for file in files:
        try:
            f = open(client.CONFIG_FOLDER + file)
            f.close()
        except IOError:
            if file == "config.yml":
                client.Log.critical(
                    "config.yml file does not exist! Please put the file from the server into the config folder!")
                exit(1)
            elif file == "server.crt.pem":
                client.Log.critical(
                    "server.crt.pem file does not exist! Please put the file from the server into the config folder!")
                exit(1)
            else:
                # generate client certificates
                createCerts()


def checkDataFolder(directory):
    if not os.path.exists(directory):
        Log.info("Data folder does not exist! Creating it...")
        os.makedirs(directory)


def copyCerts(loc):
    if not certs_copied or loc is None or loc == "None":
        return
    # check if path ends with slash
    if not loc.endswith("\\") and not loc.endswith("/"):
        loc += "/"
    try:
        shutil.copyfile(client.CONFIG_FOLDER + "client.crt.pem", loc + Tools.getHostname() + ".crt.pem")
    except FileNotFoundError:
        Log.critical(loc + " not found!")
    except FileExistsError:
        Log.critical(loc + Tools.getHostname() + ".crt.pem: File exists already!")
    Log.info("Copied client certificate to: " + loc + Tools.getHostname() + ".crt.pem")
