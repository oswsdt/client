import socket
import ssl

import client
from classes import Command
from classes import Log
from classes import PackageUtil
from classes import Protocol

# connect to server
queue = []
connected = False


def connect(config):
    Log.info("Connecting to: %s: %s" % (config['host'], str(config['port'])))
    # create ssl context
    # load trusted server cert
    context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=client.CONFIG_FOLDER + "server.crt.pem")
    # own cert
    context.load_cert_chain(certfile=client.CONFIG_FOLDER + "client.crt.pem",
                            keyfile=client.CONFIG_FOLDER + "client.key.pem")
    # server cert is a self signed cert. do not check if hostname is correct
    context.check_hostname = False
    context.verify_mode = ssl.CERT_REQUIRED
    # create socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ws = context.wrap_socket(s)
    # connecting
    try:
        ws.connect((config['host'], config['port']))
        ws.settimeout(1)  # 1 ms timeout for listening
        Log.info("Connected!")
        global connected
        global queue
        connected = True
        while True:
            if len(queue) == 0:
                # nothing to send, listen
                try:
                    data = ws.recv(Protocol.MAX_PACKAGE_LEN)
                    if not data:
                        # broken pipe, timeout or disconnect
                        break
                    json_data = PackageUtil.getJSONFromBytes(data)
                    # debug
                    print(json_data)
                    Command.handle(json_data)
                    # process packages
                except socket.timeout:
                    # is okay, continue reading and checking if something is to send...
                    pass
            else:
                # data need to get sent
                for package in queue:
                    ws.sendall(package)
                    queue.remove(package)
    except socket.timeout:
        # socket disconnected, timeout, ...
        Log.critical("Socket closed")


def sendPackage(json):
    queue.append(PackageUtil.getBytesFromJSON(PackageUtil.preparePackage(json)))
