import os
import platform
import socket

import pythoncom  # only windows
import wmi


def getArchitectureInt():
    # ToDo ARM64 check
    if platform.architecture()[0] == "32bit":
        # system or python is 32 bit based
        return 0
    # fallback: 64bit
    return 1


def getHostname():
    return socket.gethostname()


def getTotalPhysicalMemory():
    pythoncom.CoInitialize()
    comp = wmi.WMI()
    for i in comp.Win32_ComputerSystem():
        return round(int(i.TotalPhysicalMemory) / 1024 / 1024)  # size in mbyte


def getWindowsDomainName():
    return os.environ['userdomain']
