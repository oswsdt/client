import os
import platform
from _thread import *

from classes import Application
from classes import Log
from classes import SocketStuff
from classes import Tools


def sendGeneralInfoToServer():
    data = {0: {"id": 0}}
    data[0]["command"] = ClientToServerCommand.SEND_GENERAL_INFO_TO_SERVER
    data[0]["data"] = {}
    data[0]["data"]["os"] = platform.platform()
    data[0]["data"]["architecture"] = platform.architecture()[0]
    data[0]["data"]["device-name"] = Tools.getHostname()
    data[0]["data"]["memory"] = Tools.getTotalPhysicalMemory()
    data[0]["data"]["domain"] = Tools.getWindowsDomainName()
    SocketStuff.sendPackage(data)


def sendStatusInfoToServer():
    data = {0: {"id": 0}}
    data[0]["command"] = ClientToServerCommand.SEND_STATUS_INFO_TO_SERVER
    # ToDo
    SocketStuff.sendPackage(data)


def requestSoftwareRepository():
    data = {0: {"id": 0}}
    data[0]["command"] = ClientToServerCommand.GET_SOFTWARE_REPOSITORY
    SocketStuff.sendPackage(data)


def handle(data):
    try:
        for cmd in data["data"].values():
            print("Command id=%s requested" % (cmd["command"]))
            if cmd["command"] == ServerToClientCommand.SET_STATUS_ON_CLIENT:
                # update device info
                if cmd["data"]["status"] == 0:
                    # server said, we should check for software updates
                    requestSoftwareRepository()
                elif cmd["data"]["status"] == 1 or cmd["data"]["status"] == 2:
                    # reboot/shutdown
                    command = "shutdown "
                    if cmd["data"]["status"] == 1:
                        command += "-s"
                    elif cmd["data"]["status"] == 2:
                        command += "-r"
                        # shutdown
                    if cmd["data"]["force"]:
                        command += " -f -t 0"
                    os.system(command)

                elif cmd["data"]["status"] == 6:
                    command = "gpupdate"
                    if cmd["data"]["force"]:
                        command += " /force"
                    os.system(command)

                elif cmd["data"]["status"] == 4:
                    # logoff all clients
                    command = "logoff console"
                    os.system(command)

                print("Command executed!")
            elif cmd["command"] == ServerToClientCommand.RET_SOFTWARE_REPOSITORY:
                start_new_thread(Application.install, (cmd["data"],))
            else:
                print("Unknown command!")
    except (KeyError, TypeError):
        Log.socket("Server sent invalid package!")


class ServerToClientCommand:
    SET_STATUS_ON_CLIENT = 0
    RET_SOFTWARE_REPOSITORY = 1


class ClientToServerCommand:
    SEND_GENERAL_INFO_TO_SERVER = 0
    SEND_STATUS_INFO_TO_SERVER = 1
    GET_SOFTWARE_REPOSITORY = 2
