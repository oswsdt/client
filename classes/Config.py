import sys

import yaml

from classes import Log


def loadConfig(config_file):
    file = open(config_file, "r")
    try:
        config = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
        fields = ['host', 'port', 'data-path', 'copy-cert-to-folder-after-setup']
        for field in fields:
            if config[field]:
                continue
        return config
    except yaml.YAMLError:
        Log.critical("Error reading config.yml!")
        sys.exit(1)
