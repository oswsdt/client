import datetime
from enum import Enum


class LogLevel(Enum):
    INFO = 1,
    AUTHENTICATION = 2,
    WARNING = 3,
    CRITICAL = 4,
    DEBUG = 5,
    CLIENTS = 6
    SOCKET = 7


# log: LogLevel and message
def log(lvl, message):
    # print with additional information
    print('[' + lvlToString(lvl) + "] [" + datetime.datetime.now().strftime("%d.%m. %Y - %H:%M %S") + "]: " + message)
    # ToDo: log to file


def lvlToString(lvl):
    levels = {
        LogLevel.INFO: "INFO",
        LogLevel.AUTHENTICATION: "AUTHENTICATION",
        LogLevel.WARNING: "WARNING",
        LogLevel.CRITICAL: "CRITICAL",
        LogLevel.DEBUG: "DEBUG",
        LogLevel.CLIENTS: "CLIENTS",
        LogLevel.SOCKET: "SOCKET"
    }
    return levels.get(lvl, "OTHER")


# log methods

def info(m):
    log(LogLevel.INFO, m)


def auth(m):
    log(LogLevel.AUTHENTICATION, m)


def warn(m):
    log(LogLevel.WARNING, m)


def critical(m):
    log(LogLevel.CRITICAL, m)


def debug(m):
    log(LogLevel.DEBUG, m)


def client(m):
    log(LogLevel.CLIENTS, m)


def socket(m):
    log(LogLevel.CLIENTS, m)
