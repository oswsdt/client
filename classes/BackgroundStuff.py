import time

from classes import Command


def thread():
    # connected. send general infos once
    Command.sendGeneralInfoToServer()
    Command.requestSoftwareRepository()
    while True:
        # does alive stuff and status reporting later
        Command.sendStatusInfoToServer()
        time.sleep(10)
