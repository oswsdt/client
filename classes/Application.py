import configparser
import os
import shutil
import subprocess
import zipfile

import winapps

import client
from classes import Log


def install(data):
    for application in data:
        # check if is already installed
        # generate full name by the given format
        full_wname = application["wnameformat"] \
            .replace("%wname%", application["wname"]) \
            .replace("%version%", application["version"]) \
            .replace("%wversion%", application["wversion"])

        installed = False
        # get all installed applications by that name
        installed_applications = winapps.list_installed()
        # search in all installed application
        for installed_application in installed_applications:
            if installed_application.publisher == application["wauthor"] and \
                    installed_application.version == application["wversion"] and \
                    installed_application.name == full_wname:
                installed = True
                break
        if installed:
            # already installed, skipping installation
            continue
        # not installed, install it
        base_path = client.config['data-path'] + application["wname"] + "." + application['version']
        filename, file_extension = os.path.splitext(application["path"])
        installer_path = base_path + file_extension
        Log.info("Copying " + application["path"] + " to data folder...")
        try:
            shutil.copyfile(application["path"], installer_path)
        except shutil.SameFileError:
            pass
        Log.info("Copied " + application["path"] + " to data folder!")

        prefix = ""
        # check if we need to extract it
        if file_extension == ".zip":
            Log.info("Installer is a archive. Decompressing it...")
            os.mkdir(base_path)
            zipfile.ZipFile(installer_path, 'r').extractall(base_path)
            try:
                f = open(base_path + "\\AUTORUN.INF")
                config = configparser.ConfigParser()
                config.read(f)
                installer_path = base_path + "\\" + config['autorun'].get('open')
                f.close()
            except IOError:
                # no autorun.inf: try using setup.exe
                # change to setup.exe
                installer_path = base_path + "\\setup.exe"
        elif file_extension == ".msi":
            # special rules here
            prefix = "msiexec.exe /i "
        elif file_extension == ".msu":
            # it is not recommended to install windows updates on this way!
            # currently the check if it is installed is only for program not for updates =>
            # an update gets reinstalled very often:(
            prefix = "wusa.exe "

        # install it now
        Log.info("Installing " + application["wname"] + " now...")
        FNULL = open(os.devnull, 'w')
        args = prefix + "\"" + installer_path + "\" " + application["parameters"]
        exitcode = subprocess.call(args, stdout=FNULL, stderr=FNULL, shell=False)

        # check exit code
        if exitcode == 0:
            Log.info("Installation of " + application["wname"] + " completed")
            os.remove(installer_path)
        else:
            Log.info("Installation of " + application["wname"] + " failed. Exit code: " + str(exitcode))
