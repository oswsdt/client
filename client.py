#!/usr/bin/python
from _thread import *

from classes import BackgroundStuff
from classes import Config
from classes import Log
from classes import Setup
from classes import SocketStuff

CONFIG_FOLDER = 'C:\\Program Files (x86)\\OSWSDT\\config\\'
VERSION = 1
config = Config.loadConfig(CONFIG_FOLDER + "config.yml")


# main
def main():
    # main
    global config

    Log.info("Starting client (v" + str(VERSION) + ")...")
    Setup.setup()
    Setup.copyCerts(config['copy-cert-to-folder-after-setup'])
    Setup.checkDataFolder(config['data-path'])
    start_new_thread(BackgroundStuff.thread, ())
    SocketStuff.connect(config)


if __name__ == '__main__':
    main()
